//The function takes user data object as input and returns an array of usernames sorted by designation and age.
function sortUsers(userData) {
  // Get an array of user names from the user data object
  let users = Object.keys(userData);

  // Use the sort method to sort users based on their designation and age
  let result = users.sort((a, b) => {
    // Extract the first word of the designation for comparison
    let aDesignation = userData[a].desgination.split(" ");
    let bDesignation = userData[b].desgination.split(" ");

    // Extract age for comparison
    let aAge = userData[a].age;
    let bAge = userData[b].age;

    // Compare designations
    if (aDesignation[0] === bDesignation[0]) {
      // If designations are the same, compare by age in descending order
      if (aAge < bAge) {
        return 1;
      } else {
        return -1;
      }
    }

    // Compare 'Intern' and 'Senior' designations
    if (aDesignation[0] === "Intern" || bDesignation[0] === "Senior") {
      return 1; // Move 'Intern' to the end or 'Senior' to the beginning
    } else if (aDesignation[0] === "Senior" || bDesignation[0] === "Intern") {
      return -1; // Move 'Senior' to the beginning or 'Intern' to the end
    }
  });

  // Return the array of usernames sorted by designation and age
  return result;
}

// Export the sortUsers function to make it available for use in other files
module.exports = sortUsers;
