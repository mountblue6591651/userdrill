// Import the userData object from the '1-users.cjs' file
const userData = require("../1-users.cjs");

// Import the 'getUserByInterest' function from the 'problem1.js' file
const getUserByInterest = require("../problem1.js");

// Specify the interest you want to search for
let interests = "Video Games";

// Call the 'getUserByInterest' function with the user data and the specified interest
let result = getUserByInterest(userData, interests);

// Log the result (array of usernames with the specified interest) to the console
console.log(result);
