// Import the 'userData' object from the '1-users.cjs' file
const userData = require("../1-users.cjs");

// Import the 'sortUsers' function from the 'problem3.js' file
const sortUsers = require("../problem3.js");

// Call the 'sortUsers' function with the user data
let sortedUsers = sortUsers(userData);

// Log the result (array of usernames sorted by designation and age) to the console
console.log(sortedUsers);
