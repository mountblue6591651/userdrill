// Import the 'userData' object from the '1-users.cjs' file
const userData = require("../1-users.cjs");

// Import the 'groupUsers' function from the 'problem5.js' file
const groupUsers = require("../problem5.js");

// Call the 'groupUsers' function with the user data
let result = groupUsers(userData);

// Log the result (object grouping usernames by programming language in their designation) to the console
console.log(result);
