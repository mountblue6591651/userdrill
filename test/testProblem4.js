// Import the 'userData' object from the '1-users.cjs' file
let userData = require("../1-users.cjs");

// Import the 'getUserByCountry' function from the 'problem2.js' file
let getUserByCountry = require("../problem2.js");

// Specify the country you want to search for
let countryToSearch = "Germany";

// Call the 'getUserByCountry' function with the user data and the specified country
let users = getUserByCountry(userData, countryToSearch);

// Log the result (array of usernames from the specified country) to the console
console.log(users);
