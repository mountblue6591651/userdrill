/* Function takes user data object and a country as input,
 returns an array of usernames that match the specified country */
function getUserByCountry(userData, country) {
  // Get an array of user names from the user data object
  let users = Object.keys(userData);

  // Use the filter method to find users with the specified country
  let result = users.filter((user) => {
    // Check if the user's nationality matches the specified country
    if (userData[user].nationality === country) {
      // If true, include the user in the result array
      return true;
    }
  });

  // Return the array of usernames matching the specified country
  return result;
}

// Export the getUserByCountry function to make it available for use in other files
module.exports = getUserByCountry;
