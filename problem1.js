/* The function takes user data object and an interest as input,
 returns an array of usernames that have the specified interest */
function getUserByInterest(userData, interest) {
  // Get an array of user names from the user data object
  let users = Object.keys(userData);

  // Use the filter method to find users with the specified interest
  let result = users.filter((user) => {
    // Check if the user's interests array includes the specified interest
    if (userData[user].interests[0].includes(interest)) {
      // If true, include the user in the result array
      return true;
    }
  });

  // Return the array of usernames with the specified interest
  return result;
}

// Export the getUserByInterest function to make it available for use in other files
module.exports = getUserByInterest;
