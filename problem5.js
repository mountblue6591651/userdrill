// The function takes user data object as input and returns an object grouping usernames by programming language in their designation.
function groupUsers(userData) {
  // Get an array of user names from the user data object
  let users = Object.keys(userData);

  // Use the reduce method to group users by programming language in their designation
  let result = users.reduce(
    (acc, user) => {
      // Check if the designation includes 'Golang'
      if (userData[user].desgination.includes("Golang")) {
        acc["Golang"].push(user);
      }
      // Check if the designation includes 'Python'
      else if (userData[user].desgination.includes("Python")) {
        acc["Python"].push(user);
      }
      // Check if the designation includes 'Javascript'
      else if (userData[user].desgination.includes("Javascript")) {
        acc["Javascript"].push(user);
      }

      // Return the accumulator in each iteration
      return acc;
    },
    {
      Python: [], // Initialize an array for Python users
      Javascript: [], // Initialize an array for Javascript users
      Golang: [], // Initialize an array for Golang users
    }
  );

  // Return the object grouping usernames by programming language in their designation
  return result;
}

// Export the groupUsers function to make it available for use in other files
module.exports = groupUsers;
