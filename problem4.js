// The function takes user data object and a degree as input,
// returns an array of usernames that match the specified degree.
function getUserByDegree(userData, degree) {
  // Get an array of user names from the user data object
  let users = Object.keys(userData);

  // Use the filter method to find users with the specified degree
  let result = users.filter((user) => {
    // Check if the user's qualification matches the specified degree
    if (userData[user].qualification === degree) {
      // If true, include the user in the result array
      return true;
    }
  });

  // Return the array of usernames matching the specified degree
  return result;
}

// Export the getUserByDegree function to make it available for use in other files
module.exports = getUserByDegree;
